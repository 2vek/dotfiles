" disable vi support
set nocompatible

" autoload changed files
set autoread

" show filename in window title
set title

" set encoding
set encoding=utf-8

" directories for swp files
set backupdir=~/.vim//,/var/tmp//,.
set directory=~/.vim//,/var/tmp//,.

" enable mouse support
set mouse=a

" show relative line numbers
set number
set relativenumber

" search in file
set hlsearch   					"highlight searches
nnoremap <esc><esc> :silent! nohlsearch<cr> 	"dehightlight search with escape
set incsearch  					"search incrementally
set ignorecase 					"ignore case in search patterns
set smartcase  					"unignore case on capital letter

" indenting
set copyindent
set smarttab
set autoindent
set smartindent

" enable theme
syntax enable

" load plugin and indent setting for detected filetypes
filetype plugin indent on

" common typing error
command WQ wq
command Wq wq
command W w
command Q q

" copy and paste on wayland
if executable('wl-paste')
	xnoremap "+y y:call system("wl-copy", @")<cr>
	nnoremap "+p :let @"=substitute(system("wl-paste --no-newline"), '<C-v><C-m>', '', 'g')<cr>p
	nnoremap "*p :let @"=substitute(system("wl-paste --no-newline --primary"), '<C-v><C-m>', '', 'g')<cr>p
endif

" directory explorer
let g:netrw_banner = 0
let g:netrw_list_hide = netrw_gitignore#Hide()
let g:netrw_list_hide .= ',.git\/,__pycache__,.venv,node_modules'
let g:netrw_bufsettings = 'noma nomod nobl nowrap ro'
