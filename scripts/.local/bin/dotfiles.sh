#!/bin/bash

#
# put dotfiles to their right places
#

# check if stow is installed
! type stow &> /dev/null && echo "install 'stow' first." && exit

# assumimg dotfiles are in ~/.dotfiles.
cd $HOME/.dotfiles

print_usage() {
	echo "Usage: ${0##*/} (ln|rm|re) (all|<dirs>)"
	for dir in */; do DIRS+="${dir::-1} "; done
	printf "dirs:\n    ${DIRS}\n"
	exit
}

# if less command line arguments given
[[ $# -lt 2 ]] && print_usage  && exit

# to avoid seeing annoying perl issue in stow.pm line 1736.
_cmd() { stow -v $1 $2 2>&1 >/dev/null | grep -v 1736; }

case "$1" in
	'ln')	PARAM='-S' ;;
	'rm')	PARAM='-D' ;;
	're')	PARAM='-R' ;;
	*)	print_usage
esac

stow_cmd() {
	case "$1" in
	'all')
		for dir in */ ; do
			stow_cmd ${dir::-1}
		done
		;;
	*)
		[[ ! -d $1 ]] && print_usage
		_cmd $PARAM $1
	esac
}

stow_cmd $2
