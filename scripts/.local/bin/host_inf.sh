#!/bin/bash

# this scripts output server geolocation, reverse dns, ASN
# create to replace 'worldip' addons in firefox.

trap 'rm -f /tmp/host-inf' INT EXIT TERM

[[ -z "$1" ]] && echo "USAGE: $0 domain" && exit

if_ip() {
	echo $1 | grep -Eo '[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}'
}

ip_asn() {
	[ -f "/tmp/host-inf" ] && return || touch "/tmp/host-inf"
	whois -h whois.cymru.com " -f $1" | cut -d'|' -f3 | cut -c2-
}

fetch() { # $1: hostname, $2: ip addr
	if_ip $2 >/dev/null || { printf "%s\n\n" $2 && return; }
	ip_asn $2
	COUNTRY=$(curl -s https://api.wipmania.com/${2}?${1})
	RDNS=$(nslookup $2 | grep arpa |  grep -oE '[^ ]+$')
	printf "%-2s %-15s %s\n" $COUNTRY $2 $RDNS
}

# export don't work in zsh, parallel need functions to be exported
SHELL=/bin/bash
export -f fetch if_ip ip_asn

dig A +short $1 | parallel -k fetch $1
