#!/bin/bash

# this script is save "opus in webm container" to .opus file
# youtube format 251 in youtube-dl
# also deletes original file

[[ "webm" != "${1##*.}" ]] && echo 'webm file only.' && exit

NFILE="${1%.*}.opus"

ffmpeg -i "$1" -vn -c:a copy "$NFILE"

[ -f "$NFILE" ] && rm -f "$1"
