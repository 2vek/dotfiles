#!/usr/bin/env python3

import sys

if not len(sys.argv) > 1:
    exit()

if sys.argv[1] == '-h':
    help_msg = """Get all links in json link of subreddit
{script} [options] subreddit_json_links

options:
-n          links in saperate line each, instead of in a single line
-h          display the help""".format(script=sys.argv[0])
    print(help_msg)
    exit()

rlinks = []
splitt = ' '

for index, link in enumerate(sys.argv, start=1):
    if index > 1:
        import time
        time.sleep(3)

    if link == '-n':
        splitt = '\n'

    if 'reddit.com/r/' not in link and '.json' not in link:
        continue

    import urllib.request as request
    resp = request.urlopen(link)
    data = resp.read()
    subr = data.decode('utf-8')

    import json
    subrjson = json.loads(subr)
    for item in subrjson['data']['children']:
        if item['data']['is_self']:
            continue
        rlinks.append(item['data']['url'])

print(splitt.join(rlinks))
