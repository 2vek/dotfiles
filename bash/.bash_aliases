alias q='exit'

alias jukempv="mpv --profile=jukebox"
alias hmpv="mpv --profile=hq"
alias fmpv="mpv --profile=float"

function ips() {
	echo $(ip -br a | grep UP | tr -s ' ' | cut -d' ' -f3 | cut -d'/' -f1)
	echo $(curl -s http://icanhazip.com/)
}

alias venv="[ -d .venv ] && source .venv/bin/activate || echo 'no .venv/'"
