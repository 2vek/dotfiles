# Dotfiles

my dotfiles and scripts


## Setup

1. Install `GNU Stow`
2. clone this repo
    ```
    git clone git@github.com:2vek/dotfiles.git ~/.dotfiles
    ```
3. setup local bin dir
    * create dir `mkdir -p ~/.local/bin`
    * add `$HOME/.local/bin` to `$PATH`
4. symlink scripts
    ```
    ~/.dotfiles/scripts/.local/bin/dotfiles.sh ln scripts
    ```
5. run `dotfiles.sh` for usage.


> All symlinks are relative to the location of repo.
> Remove symlinks before moving or deleting directories and files


## Instruction

setup might be needed for some of these configs.

### bash

add snippets to `~/.bashrc`
```bash
if [ -f $HOME/.bash_aliases ]; then
	. $HOME/.bash_aliases
fi
```

### pulseaudio

to load config immediately, run `pulseaudio --check && pulseaudio --kill`

### mpv

download latest [mpris.so](https://github.com/hoyon/mpv-mpris/releases) at `~/.config/mpv/scripts/`

### youtube-dl

create config directory first, `~/.config/youtube-dl`

### vim

create cache dir `~/.cache/vim`

also create symlink of vimrc to /root/ for sudo

install `wl-clipboard` to copy paste in wayland
